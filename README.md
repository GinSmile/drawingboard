DrawingBoard
========

功能介绍

 * 本程序为简易画板程序，实现了画直线，圆，矩形等图形。
 * 可以撤销上一步，也可以将图形全部清除。
 * 另外，本程序还实现了文件的保存和打开，用户可以将图形保存，或打开某个保存过的文件。

效果图如下：


   ![效果图2](https://github.com/GinSmile/DrawingBoard/blob/master/screenshot/one.png?raw=true)  
