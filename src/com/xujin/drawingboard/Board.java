/*
 * Author:GinSmile
 * Date:2015.01.01
 * Describe:
 * 			1.本程序为简易画板程序，实现了画直线，圆，矩形等图形。
 * 			2.可以撤销上一步，也可以将图形全部清除。
 *          3.另外，本程序还实现了文件的保存和打开，用户可以将图形保存，或打开某个保存过的文件。
 */


package com.xujin.drawingboard;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class Board{
	public static void main(String args[]){
		EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				BoardFrame bf = new BoardFrame();
				bf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				bf.setVisible(true);
			}
			
		});
	}
}
