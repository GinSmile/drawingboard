package com.xujin.drawingboard;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

//画板Frame
class BoardFrame extends JFrame{
	public static final int DEFAULT_WIDTH = 500;
	public static final int DEFAULT_HEIGHT = 400;
	
	private JPanel buttonPanel;
	private String type = "line";
	private int count = 0;
	private double x1,x2,y1,y2;
	private Container c;
	private ArrayList<Graph> array = new ArrayList<Graph>();
	// 定义保存路径  
    String path = "save.dat";  

	public BoardFrame(){
		setTitle("Java画板");
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		
		//菜单
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		JMenu fileMenu = new JMenu("文件");
		JMenu helpMenu = new JMenu("帮助");
		
		JMenuItem openItem = new JMenuItem("打开文件");
		JMenuItem saveItem = new JMenuItem("保存文件");
		JMenuItem helpItem = new JMenuItem("帮助信息");
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		helpMenu.add(helpItem);
		
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		
		openItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				chooser.setSelectedFile(new File("save.dat"));

				chooser.showOpenDialog(c);
				String filename = chooser.getSelectedFile().getPath();
				try {
					array = FileProcess.open(filename);
					count = 3;
					repaint();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		saveItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					JFileChooser chooser = new JFileChooser();
					chooser.setCurrentDirectory(new File("."));
					chooser.showSaveDialog(c);
					String filename = chooser.getSelectedFile().getPath();
					FileProcess.save(filename, array);

					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		helpItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("OMG!");
				JOptionPane.showMessageDialog(null, "本程序为简易画板程序，在不同位置分别点击一次可以得到直线、圆形、矩形。", "Powered by GinSmile",JOptionPane.ERROR_MESSAGE);  
			}
			
		});
		
		

		
		
		
		//add buttons
	    JButton line = new JButton("直线");
		JButton circle = new JButton("圆形");
		JButton rect = new JButton("矩形");
		JButton erase = new JButton("撤销");
		JButton eraseAll = new JButton("重置画板");
		
		//add buttons to Panel
		buttonPanel = new JPanel();
		buttonPanel.add(line);
		buttonPanel.add(circle);
		buttonPanel.add(rect);
		buttonPanel.add(erase);
		buttonPanel.add(eraseAll);
		
				
		
		//create button actions
		line.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				type = "line";
				count = 1;
			}
			
		});
		circle.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				type = "circle";
				count = 1;
			}
			
		});
		rect.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				type = "rect";
				count = 1;
			}
			
		});
		
		erase.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				array.remove(array.size() - 1);
				count = 3;//避免出现重绘失败
				repaint();
				

			}
			
		});
		
		eraseAll.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				array.clear();
				count = 3;
				repaint();
			}
			
		});
		
		
		
		c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(buttonPanel, BorderLayout.AFTER_LAST_LINE);
		
		//add Panel to Frame
		//add(buttonPanel);
		addMouseListener(new DrawListener());

				
		
		}


	//鼠标点击监听器
	class DrawListener extends MouseAdapter{
		public void mouseClicked(MouseEvent e){
			if(count == 1){
				x1=e.getX();
				y1=e.getY();
				count=2;
				//System.out.println("x1="+x1+", y1="+y1);
			}else if(count == 2){
				x2=e.getX();
				y2=e.getY();
				//System.out.println("x2="+x2+", y2="+y2);
				
				repaint();
			}
		}
		
	
	}

	
	
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D)g;
		
		if(count==2 || count == 3) {
			if(count == 2){
				Graph graph = new Graph(x1,x2,y1,y2,type);//仅当count＝＝2，即第二次点击画板时才添加一个图形，当count＝＝3时是撤销操作！
				array.add(graph);
			}
			System.out.println("ok!!" + array.size());
			for(Graph myGraph:array){
				x1 = myGraph.getX1();
				x2 = myGraph.getX2();
				y1 = myGraph.getY1();
				y2 = myGraph.getY2();
				if(myGraph.getType().equals("line")) {
					g2.draw(new Line2D.Double(x1,y1,x2,y2));
				}else if(myGraph.getType().equals("circle")){
					Ellipse2D circle = new Ellipse2D.Double();
					double radius = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
					circle.setFrameFromCenter(x1,y1,x1+radius,y1+radius);
					g2.draw(circle);
				}else if(myGraph.getType().equals("rect")){
					double leftX, topY, width, height;
					leftX = x1<x2?x1:x2;
					topY = y1<y2?y1:y2;
					width = Math.abs(x1 - x2);
					height = Math.abs(y1 - y2);
					Rectangle2D rect = new Rectangle2D.Double(leftX,topY,width,height);
					g2.draw(rect);
				}
			}

		}
		
		count=1;
	}
	
}
