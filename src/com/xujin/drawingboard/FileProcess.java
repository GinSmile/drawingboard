package com.xujin.drawingboard;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileProcess {
	/**
	 * 保存到文件
	 * @throws IOException 
	 */
	public static void save(String path,ArrayList<Graph> list) throws IOException{
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path));
			out.writeObject(list);
			out.flush();
			out.close();
			} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * 读取文件
	 * @throws IOException 
	 */
	public  static ArrayList<Graph> open(String path) throws IOException{
	
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(path));
			ArrayList<Graph> list = (ArrayList<Graph>)in.readObject();
			in.close();
			return list;
			} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		return null;
	}
}
